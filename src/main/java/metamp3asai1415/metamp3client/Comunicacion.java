/*
 * Cliente de la herramienta de compartición de archivos MP3
 * y edición de metadatos.
 * Aplicaciones y Servicios Avanzados en Internet,
 * Máster en Ingeniería Informática, curso 2014-2015,
 * Universidad de Valladolid.
 *
 * # Autores
 * Cerezo Redondo,		Borja.
 * Roda Suárez,			Daniel.
 * Tejedor García,		Cristian.
 */
package metamp3asai1415.metamp3client;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 1.0-FINAL
 */
public class Comunicacion {

    private static final String HOST_URL = "http://pasarela.lab.inf.uva.es:30013/";
    private static final String DIRECTORIO_ARCHIVOS = "./mp3/";
    private static final int MAX_INTENTOS = 10;
    private static int numIntentos = 0;
    private static final int POS_NOT_FOUND_IN_ARRAY = -1;
    private static boolean online = true;

    public static void pasarOnline() throws IOException, JSONException, UnsupportedEncodingException, ExcepcionMetaMp3 {
        online = true;
        sincronizar(listaCancionesJSONToListaPistas(InteraccionJSONLocal.leerFichero()));
    }

    public static void pasarOffline() {
        online = false;
    }

    public static List<Pista> verLista() throws JSONException, ExcepcionMetaMp3 {
        String url = HOST_URL + ConstantesJSON.OPCION_KEY_VER_LISTA;
        final JSONArray listaCancionesActualizadasJSON = new JSONArray("" + HttpConnection.conexionHttp(url, "GET"));
        List<Pista> listaRecibida = listaCancionesJSONToListaPistas(listaCancionesActualizadasJSON);
        imprimeLista(listaRecibida);

        return listaRecibida;
    }

    public static List<Pista> verListaLocal() throws IOException, JSONException, ExcepcionMetaMp3 {
        List<Pista> listaLocal = listaCancionesJSONToListaPistas(InteraccionJSONLocal.leerFichero());
        imprimeLista(listaLocal);

        return listaLocal;
    }

    public static void sincronizar(Pista cancion) throws JSONException, UnsupportedEncodingException, IOException, ExcepcionMetaMp3 {

        if (cancion == null) {
            throw new ExcepcionMetaMp3("No se ha facilitado ninguna pista a sincronizar.");
        }
        List<Pista> canciones = new ArrayList<Pista>();
        canciones.add(cancion);
        sincronizar(canciones);
    }

    public static void sincronizar(List<Pista> canciones) throws JSONException, UnsupportedEncodingException, IOException, ExcepcionMetaMp3 {
        if (canciones == null || canciones.size() == 0) {
            throw new ExcepcionMetaMp3("No se ha facilitado ninguna pista a sincronizar.");
        }
        int numCanciones = canciones.size();
        for (int i = 0; i < numCanciones - 1; i++) {
            for (int j = i + 1; j < numCanciones; j++) {
                if (canciones.get(i).getId() == canciones.get(j).getId()) {
                    throw new ExcepcionMetaMp3("La lista a sincronizar contiene canciones con ids repetidos (" + canciones.get(i).getId() + ").");
                }
            }
        }
        if (!online) {
            actualizaCanciones(canciones);
        } else {
            final JSONArray listaCancionesJSON = new JSONArray(canciones);
            final List<Integer> listaErrores = new ArrayList<Integer>();
            final List<Integer> listaCancionesError = new ArrayList<Integer>();
            String url = HOST_URL + ConstantesJSON.OPCION_KEY_SINCRONIZAR + "?" + ConstantesJSON.PISTA_KEY_SINCRONIZAR_PISTAS + "=" + URLEncoder.encode(listaCancionesJSON.toString(), "UTF-8");
            final JSONArray listaCancionesActualizadasJSON = new JSONArray("" + HttpConnection.conexionHttp(url, "GET"));
            for (int i = 0; i < numCanciones; i++) {
                final JSONObject cancionActualizadaJSON = listaCancionesActualizadasJSON.getJSONObject(i);
                int posCancion = POS_NOT_FOUND_IN_ARRAY;
                for (int j = 0; j < numCanciones && posCancion == POS_NOT_FOUND_IN_ARRAY; j++) {
                    if (canciones.get(j).getId() == cancionActualizadaJSON.getInt(ConstantesJSON.PISTA_API_KEY_ID)) {
                        posCancion = j;
                    }
                }
                if (!actualizaCancion(cancionActualizadaJSON)) {
                    listaCancionesError.add(posCancion);
                    listaErrores.add(i);
                }
            }
            int numErrores = listaErrores.size();
            if (numErrores > 0) {
                numIntentos++;
                if (numIntentos == MAX_INTENTOS) {
                    for (int i = 0; i < numErrores; i++) {
                        //Gestionar información del error
                        JSONObject errorJSON = listaCancionesActualizadasJSON.getJSONObject(listaErrores.get(i));
                        throw new ExcepcionMetaMp3(new StringBuilder("\n").append("Error sincronizando canción: ").append(canciones.get(listaCancionesError.get(i)).getNombreCancion())
                                .append("\n\t").append(errorJSON.getString(ConstantesJSON.PISTA_KEY_ESTADO)).append(": ").append(errorJSON.getString(ConstantesJSON.PISTA_KEY_ESTADO_MENSAJE)).append("\n\n").toString());
                    }
                } else {
                    List<Pista> cancionesAReintentar = new ArrayList<Pista>();
                    for (int posCancion : listaCancionesError) {
                        cancionesAReintentar.add(canciones.get(posCancion));
                    }
                    sincronizar(cancionesAReintentar);
                }
            }
        }
    }

    public static void descargarArchivo(final Pista cancion, final int zona) throws JSONException, UnsupportedEncodingException, ExcepcionMetaMp3 {
        if (cancion == null) {
            throw new ExcepcionMetaMp3("No se ha facilitado ninguna pista a descargar.");
        }
        final List<Pista> canciones = new ArrayList<Pista>();
        canciones.add(cancion);
        descargarArchivo(canciones, zona);
    }

    public static void descargarArchivo(final List<Pista> canciones, int zona) throws JSONException, UnsupportedEncodingException, ExcepcionMetaMp3 {
        if (canciones == null || canciones.size() == 0) {
            throw new ExcepcionMetaMp3("No se ha facilitado ninguna pista a descargar.");
        } else {
            String url = HOST_URL + ConstantesJSON.OPCION_KEY_DESCARGAR + "?" + ConstantesJSON.PISTA_API_KEY_ID + "=" + pistasIdToUrlFormatString(canciones) + "&" + ConstantesJSON.PISTA_API_KEY_ZONA + "=" + zona;

            final JSONArray listaCancionesDescargar = new JSONArray("" + HttpConnection.conexionHttp(url, "GET"));

            int numCancionesDescargar = listaCancionesDescargar.length();
            for (int i = 0; i < numCancionesDescargar; i++) {
                Pista cancionDescargar = cancionJSONtoPista(listaCancionesDescargar.getJSONObject(i));
                compruebaCreaSiNoExiste();
                System.out.println("--> Descargando "+cancionDescargar.getNombreCancion()+"... \n\n");
                GestorWebDAV.descarga(cancionDescargar.getUrl(), DIRECTORIO_ARCHIVOS + cancionDescargar.getNombreCancion() + "_" + cancionDescargar.getId() + ".mp3");
                try {
                    actualizaCancion(cancionDescargar);
                } catch (IOException ex) {
                    //Error al actualizar
                }

            }
        }
    }

    private static String pistasIdToUrlFormatString(List<Pista> canciones) {
        StringBuilder ids = new StringBuilder();
        for (Pista cancion : canciones) {
            ids.append(cancion.getId()).append(',');
        }
        return '[' + ids.substring(0, (ids.length() - 1)) + ']';
    }

    private static List<Pista> listaCancionesJSONToListaPistas(JSONArray listaCancionesJSON) throws JSONException, ExcepcionMetaMp3 {
        if (listaCancionesJSON == null || listaCancionesJSON.length() == 0) {
            return new ArrayList<Pista>();
        } else {
            List<Pista> listaPistas = new ArrayList<Pista>();
            int numCanciones = listaCancionesJSON.length();
            for (int i = 0; i < numCanciones; i++) {
                listaPistas.add(cancionJSONtoPista(listaCancionesJSON.getJSONObject(i)));
            }
            return listaPistas;
        }
    }

    private static Pista cancionJSONtoPista(JSONObject cancionJSON) throws JSONException, ExcepcionMetaMp3 {
        if (cancionJSON == null) {
            throw new ExcepcionMetaMp3("Se intenta transformar Pista un JSONObject vacío.");
        } else {
            String hash = "";
            if (cancionJSON.has(ConstantesJSON.PISTA_KEY_HASH)) {
                hash = cancionJSON.getString(ConstantesJSON.PISTA_KEY_HASH);
            }
            String url = "";
            if (cancionJSON.has(ConstantesJSON.PISTA_KEY_URL)) {
                url = cancionJSON.getString(ConstantesJSON.PISTA_KEY_URL);
            }
            return new Pista(cancionJSON.getInt(ConstantesJSON.PISTA_KEY_ID),
                    cancionJSON.getString(ConstantesJSON.PISTA_KEY_NOMBRE_CANCION),
                    cancionJSON.getString(ConstantesJSON.PISTA_KEY_ARTISTA),
                    cancionJSON.getInt(ConstantesJSON.PISTA_KEY_ID_GENERO),
                    cancionJSON.getString(ConstantesJSON.PISTA_KEY_FECHA_MOD_NOMBRE_CANCION),
                    cancionJSON.getString(ConstantesJSON.PISTA_KEY_FECHA_MOD_ARTISTA),
                    cancionJSON.getString(ConstantesJSON.PISTA_KEY_FECHA_MOD_GENERO),
                    hash, url);
        }
    }

    private static boolean actualizaCancion(JSONObject cancionJSON) throws JSONException, IOException, ExcepcionMetaMp3 {
        return actualizaCancion(cancionJSONtoPista(cancionJSON));
    }

    private static boolean actualizaCancion(Pista pista) throws IOException, JSONException, ExcepcionMetaMp3 {
        compruebaCreaSiNoExiste();
        File[] resultados = GestionArchivos.searchFileEndsWith(new File(DIRECTORIO_ARCHIVOS), "_" + pista.getId() + ".mp3");
        switch (resultados.length) {
            case 0:
                return true;
            case 1:
                int numIntentosGuardado = 0;
                while (AudioTagger.updateFichero(resultados[0].getPath(), pista) == AudioTagger.EDICION_INCORRECTA && numIntentosGuardado < MAX_INTENTOS) {
                    numIntentosGuardado++;
                }
                if (numIntentosGuardado == MAX_INTENTOS) {
                    throw new ExcepcionMetaMp3("Se ha excedido el número máximo de intentos para editar el mp3.");
                }
                String nuevoNombre = pista.getNombreCancion() + "_" + pista.getId() + ".mp3";
                if (!nuevoNombre.equals(resultados[0].getName())) {
                    numIntentosGuardado = 0;
                    File nuevoFichero = new File(DIRECTORIO_ARCHIVOS + nuevoNombre);
                    while (!resultados[0].renameTo(nuevoFichero) && numIntentosGuardado < MAX_INTENTOS) {
                        numIntentosGuardado++;
                    }
                    if (numIntentosGuardado == MAX_INTENTOS) {
                        throw new ExcepcionMetaMp3("Se ha excedido el número máximo de intentos para renombrar el mp3.");
                    }
                }
                InteraccionJSONLocal.escribirFichero(pista);
                return true;
            default:
                StringBuilder error = new StringBuilder("Error en los nombres de ficheros en el directorio .\nHay más de un archivo relacionados con el mismo id (")
                        .append(pista.getId()).append("):\n");
                for (File resultado : resultados) {
                    error.append(resultado.getName()).append("\t");
                }
                throw new ExcepcionMetaMp3(error.toString());
        }
    }

    private static void actualizaCanciones(List<Pista> listaPistas) throws IOException, JSONException, ExcepcionMetaMp3 {
        final List<Pista> listaValida = new ArrayList<Pista>();
        compruebaCreaSiNoExiste();
        File[] resultados;
        for (Pista pista : listaPistas) {
            resultados = GestionArchivos.searchFileEndsWith(new File(DIRECTORIO_ARCHIVOS), "_" + pista.getId() + ".mp3");
            switch (resultados.length) {
                case 0:
                    //Excepcion Ignorada en local
                    break;
                case 1:
                    int numIntentosGuardado = 0;
                    while (AudioTagger.updateFichero(resultados[0].getPath(), pista) == AudioTagger.EDICION_INCORRECTA && numIntentosGuardado < MAX_INTENTOS) {
                        numIntentosGuardado++;
                    }
                    if (numIntentosGuardado == MAX_INTENTOS) {
                        throw new ExcepcionMetaMp3("Se ha excedido el número máximo de intentos para editar el mp3.");
                    }
                    String nuevoNombre = pista.getNombreCancion() + "_" + pista.getId() + ".mp3";
                    if (!nuevoNombre.equals(resultados[0].getName())) {
                        numIntentosGuardado = 0;
                        File nuevoFichero = new File(DIRECTORIO_ARCHIVOS + nuevoNombre);
                        while (!resultados[0].renameTo(nuevoFichero) && numIntentosGuardado < MAX_INTENTOS) {
                            numIntentosGuardado++;
                        }
                        if (numIntentosGuardado == MAX_INTENTOS) {
                            throw new ExcepcionMetaMp3("Se ha excedido el número máximo de intentos para renombrar el mp3.");
                        }
                    }
                    listaValida.add(pista);
                default:
                //Excepcion ignorada en local. Más archivos terminados en el id de los debidos
            }
        }
        InteraccionJSONLocal.escribirFichero(listaValida);
    }

    private static void imprimeLista(List<Pista> listaRecibida) {
        if (listaRecibida != null && listaRecibida.size() > 0) {

            StringBuilder muestreo = new StringBuilder("\n");
            for (Pista pista : listaRecibida) {
                muestreo.append(ConstantesJSON.PISTA_KEY_NOMBRE_CANCION).append(": ").append(pista.getNombreCancion())
                        .append("\t[ ").append(pista.getFechaModificacionNombreCancion()).append(" ]\n")
                        .append(ConstantesJSON.PISTA_KEY_ARTISTA).append(": ").append(pista.getArtista())
                        .append("\t[ ").append(pista.getFechaModificacionArtista()).append(" ]\n")
                        .append(ConstantesJSON.PISTA_KEY_ID_GENERO).append(": ").append(pista.getIdGenero())
                        .append("\t[ ").append(pista.getFechaModificacionGenero()).append(" ]\n\n");
            }

            System.out.println(muestreo.append("\n"));
        }
    }

    private static void compruebaCreaSiNoExiste() {
        final File saveDir = new File(DIRECTORIO_ARCHIVOS);
        if (!saveDir.exists()) {
            saveDir.mkdirs();
        }

    }
}
