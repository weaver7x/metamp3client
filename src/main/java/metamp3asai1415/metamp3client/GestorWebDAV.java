/*
 * Cliente de la herramienta de compartición de archivos MP3
 * y edición de metadatos.
 * Aplicaciones y Servicios Avanzados en Internet,
 * Máster en Ingeniería Informática, curso 2014-2015,
 * Universidad de Valladolid.
 *
 * # Autores
 * Cerezo Redondo,		Borja.
 * Roda Suárez,			Daniel.
 * Tejedor García,		Cristian.
 */
package metamp3asai1415.metamp3client;

import com.github.sardine.Sardine;
import com.github.sardine.SardineFactory;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Gestiona las conexiones WebDAV.
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 1.0-FINAL
 */
public final class GestorWebDAV {

    /**
     * Suponemos que siempre somos: user: resu
     */
    private final static Sardine sardine = SardineFactory.begin("user", "resu");

    /**
     * @String uriOrigen URI con la ruta absoluta del fichero incluido: Ejemplo:
     * http://yourdavserver.com/webdav/1231.mp3
     * @String urlDestino URI absoluta con el fichero incluido al final Ejemplo:
     * c://ey/pei/pakaro_1231.mp3
     */
    static void descarga(final String uriOrigen, final String uriDestino) {
        try {
            final InputStream is = sardine.get(uriOrigen);
            // write the inputStream to a FileOutputStream
            final OutputStream outputStream =
                    new FileOutputStream(new File(uriDestino));

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = is.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
        } catch (IOException ex) {
            Logger.getLogger(GestorWebDAV.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}