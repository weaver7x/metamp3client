/*
 * Cliente de la herramienta de compartición de archivos MP3
 * y edición de metadatos.
 * Aplicaciones y Servicios Avanzados en Internet,
 * Máster en Ingeniería Informática, curso 2014-2015,
 * Universidad de Valladolid.
 * 
 * # Autores
 * Cerezo Redondo,		Borja.
 * Roda Suárez,			Daniel.
 * Tejedor García,		Cristian.
 */
package metamp3asai1415.metamp3client;


import java.util.HashMap;
import org.codehaus.jettison.json.JSONString;


/**
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public class Pista implements JSONString{

    /**
     * Identificador único de la pista.
     */
    private int id;
    /**
     * Nombre de la canción de la pista.
     */
    private String nombreCancion;
    /**
     * Nombre del artista de la canción de la pista.
     */
    private String artista;
    /**
     * Género al que pertenece la canción de la pista.
     */
    private int idGenero;
    /**
     * Fecha de modificación del nombre de la canción de la pista.
     */
    private String fechaModiNombreCancion;
    /**
     * Fecha de modificación del artista de la pista.
     */
    private String fechaModiArtista;
    /**
     * Fecha de modificación del género de la pista.
     */
    private String fechaModiGenero;
    /**
     * Hash del fichero de la pista.
     */
    private String hash;
    /**
     * URL de la zona donde se encuentra la pista.
     */
    private String url;

    /**
     * No se permite la construcción de una Pista vacía.
     */
    private Pista() {
    }

    /**
     * Constructor básico con todas las características de una Pista.
     *
     * @param id Identificador del fichero de la pista.
     * @param nombreCancion Nombre de la canción de la pista.
     * @param artista Nombre del artista de la canción de la pista.
     * @param idGenero Género al que pertenece la canción de la pista.
     * @param fechaModiNombreCancion Fecha de modificación del fichero
     * de la pista.
     * @param fechaModiArtista Fecha de modificación del artista de la
     * pista.
     * @param fechaModiGenero Fecha de modificación del género de la
     * pista.
     * @param estadoRelacionServidor Relaciona servidores con índices de
     * descargas (para la replicación) [puede ser nulo].
     * @param hash Hash del fichero de la pista [puede ser nulo].
     * @param url URL del fichero de la pista [puede ser nulo].
     */
    public Pista(final int id, final String nombreCancion, final String artista, final int idGenero,
            final String fechaModiNombreCancion,
            final String fechaModiArtista, final String fechaModiGenero,
            final String hash, final String url) {
        // TODO: programación defensiva menos estadoRelacionServidor, hash y url
        this.id = id;
        this.nombreCancion = nombreCancion;
        this.artista = artista;
        this.idGenero = idGenero;
        this.fechaModiNombreCancion = fechaModiNombreCancion;
        this.fechaModiArtista = fechaModiArtista;
        this.fechaModiGenero = fechaModiGenero;
        // Puede ser nulo
        this.hash = hash;
        // Puede ser nulo
        this.url = url;
    }

    /**
     * Obtiene el id de la pista.
     *
     * @return Id de la pila pista.
     */
    public int getId() {
        return this.id;
    }

    /**
     * Establece un nuevo valor del id de la pista.
     *
     * @param id Nuevo valor del id de la pista.
     */
    public void setId(final int id) {
        this.id = id;
    }

    /**
     * Obtiene el nombre de la canción de la pista.
     *
     * @return Nombre de la canción de la pista.
     */
    public String getNombreCancion() {
        return this.nombreCancion;
    }

    /**
     * Establece un nuevo nombre de la canción de la pista.
     *
     * @param nombreCancion Nuevo nombre a establecer.
     */
    public void setNombreCancion(final String nombreCancion) {
        this.nombreCancion = nombreCancion;
    }

    /**
     * Obtiene el mombre del artista de la canción de la pista.
     *
     * @return Nombre del artista de la canción de la pista.
     */
    public String getArtista() {
        return this.artista;
    }

    /**
     * Establece un nuevo mombre del artista de la canción de la pista.
     *
     * @param artista Nuevo mombre del artista de la canción de la pista.
     */
    public void setArtista(final String artista) {
        this.artista = artista;
    }

    /**
     * Obtiene el género al que pertenece la canción de la pista.
     *
     * @return Género al que pertenece la canción de la pista.
     */
    public int getIdGenero() {
        return this.idGenero;
    }

    /**
     * Establece un nuevo género al que pertenece la canción de la pista.
     *
     * @param idGenero Nuevo género a establecer.
     */
    public void setIdGenero(final int idGenero) {
        this.idGenero = idGenero;
    }

    /**
     * Obtiene la fecha de modificación del nombre de canción de la pista.
     *
     * @return Fecha de modificación del fichero de la pista.
     */
    public String getFechaModificacionNombreCancion() {
        return this.fechaModiNombreCancion;
    }

    /**
     * Establece una nueva fecha de modificación del nombre de canción de la
     * pista.
     *
     * @param fechaModiNombreCancion Nueva fecha de modificación a
     * establecer del nombre de la canción
     */
    public void setFechaModificacionNombreCancion(final String fechaModiNombreCancion) {
        this.fechaModiNombreCancion = fechaModiNombreCancion;
    }

    /**
     * Obtiene la fecha de modificación del artista de la pista.
     *
     * @return Fecha de modificación del artista de la pista.
     */
    public String getFechaModificacionArtista() {
        return fechaModiArtista;
    }

    /**
     * Establece una nueva fecha de modificación del artista de la pista.
     *
     * @param fechaModiArtista Nueva fecha de modificación a establecer
     * del artista.
     */
    public void setFechaModificacionArtista(final String fechaModiArtista) {
        this.fechaModiArtista = fechaModiArtista;
    }

    /**
     * Obtiene la fecha de modificación del género de la pista.
     *
     * @return Fecha de modificación del género de la pista.
     */
    public String getFechaModificacionGenero() {
        return fechaModiGenero;
    }

    /**
     * Esrablece la fecha de modificación del género de la pista.
     *
     * @param fechaModiGenero Nueva fecha de modificación a establecer
     * del género de la canción
     */
    public void setFechaModificacionGenero(final String fechaModiGenero) {
        this.fechaModiGenero = fechaModiGenero;
    }

    /**
     * Obtiene el hash del fichero de la pista.
     *
     * @return El hash del fichero de la pista.
     */
    public String getHash() {
        return hash;
    }

    /**
     * Establece un hash para la pista.
     *
     * @param hash Hash a establecer para la pista
     */
    public void setHash(final String hash) {
        this.hash = hash;
    }

    /**
     * Obtiene la URL del fichero de la pista.
     *
     * @return La URL del fichero de la pista.
     */
    protected String getUrl() {
        return url;
    }

    /**
     * Establece la URL para la pista.
     *
     * @param hash la URL a establecer para la pista
     */
    public void setUrl(final String url) {
        this.url = url;
    }

    @Override
    public String toJSONString() {
        final StringBuilder salidaJSONString = new StringBuilder ("{\"")
                .append(ConstantesJSON.PISTA_KEY_ID).append("\":").append(this.id).append(",\"")
                .append(ConstantesJSON.PISTA_KEY_NOMBRE_CANCION).append("\":\"").append(this.nombreCancion).append("\",\"")
                .append(ConstantesJSON.PISTA_KEY_ARTISTA).append("\":\"").append(this.artista).append("\",\"")
                .append(ConstantesJSON.PISTA_KEY_ID_GENERO).append("\":").append(this.idGenero).append(",\"")
                .append(ConstantesJSON.PISTA_KEY_FECHA_MOD_NOMBRE_CANCION).append("\":\"").append(this.fechaModiNombreCancion).append("\",\"")
                .append(ConstantesJSON.PISTA_KEY_FECHA_MOD_ARTISTA).append("\":\"").append(this.fechaModiArtista).append("\",\"")
                .append(ConstantesJSON.PISTA_KEY_FECHA_MOD_GENERO).append("\":\"").append(this.fechaModiGenero).append("\"}");
        return salidaJSONString.toString();
    }
}