/*
 * Cliente de la herramienta de compartición de archivos MP3
 * y edición de metadatos.
 * Aplicaciones y Servicios Avanzados en Internet,
 * Máster en Ingeniería Informática, curso 2014-2015,
 * Universidad de Valladolid.
 * 
 * # Autores
 * Cerezo Redondo,		Borja.
 * Roda Suárez,			Daniel.
 * Tejedor García,		Cristian.
 */
package metamp3asai1415.metamp3client;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;

/**
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public class AudioTagger {

    /**
     * FLAG que indica que la edicón fue correcta.
     */
    public final static byte EDICION_CORRECTA = 0;
    /**
     * FLAG que indica que la edicón fue incorrecta.
     */
    public final static byte EDICION_INCORRECTA = -1;

    public static byte updateFichero(final String ruta, final Pista pista) {
    return updateFichero(ruta, pista.getNombreCancion(), pista.getArtista(), (pista.getIdGenero() + ""));
    }

    public static byte updateFichero(final String ruta, final String nombrePista, final String artista, final String idGenero) {
        try {
            final File archivo = new File(ruta);
            final AudioFile f = AudioFileIO.read(archivo);
            final Tag tagg = (Tag) f.getTag();
            if (nombrePista != null) {
                tagg.setField(FieldKey.TITLE, nombrePista);
            }
            if (artista != null) {
                tagg.setField(FieldKey.ARTIST, artista);
            }
            if (idGenero != null) {
                tagg.setField(FieldKey.GENRE, idGenero);
            }

            AudioFileIO.write(f);
        } catch (final Exception ex) {
            Logger.getLogger(AudioTagger.class.getName()).log(Level.SEVERE, null, ex);
            return EDICION_INCORRECTA;
        }
        return EDICION_CORRECTA;
    }
}
