/*
 * Cliente de la herramienta de compartición de archivos MP3
 * y edición de metadatos.
 * Aplicaciones y Servicios Avanzados en Internet,
 * Máster en Ingeniería Informática, curso 2014-2015,
 * Universidad de Valladolid.
 *
 * # Autores
 * Cerezo Redondo,		Borja.
 * Roda Suárez,			Daniel.
 * Tejedor García,		Cristian.
 */
package metamp3asai1415.metamp3client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 1.0-FINAL
 */
public class InteraccionJSONLocal {

    private static final String RUTA_JSON = "./mp3/bbdd.json";

    protected static JSONArray leerFichero() throws IOException, JSONException {
        String leido = readFile(RUTA_JSON);
        if (leido.length() > 0) {
            return new JSONArray(leido);
        }
        return new JSONArray();
    }

    protected static void escribirFichero(final List<Pista> listaPistas) throws IOException, JSONException {
        JSONArray contenido = leerFichero();
        for (Pista pista : listaPistas) {
            actualizaPista(contenido, pista);
        }
        saveFile(contenido);
    }

    protected static void escribirFichero(final Pista pista) throws IOException, JSONException {
        JSONArray contenido = leerFichero();
        actualizaPista(contenido, pista);
        saveFile(contenido);
    }

    protected static JSONArray actualizaPista(final JSONArray contenido, Pista pista) throws IOException, JSONException {
        int id = pista.getId();
        int tamContenido = contenido.length();
        boolean noEncontrado = true;
        for (int i = 0; i < tamContenido && noEncontrado; i++) {
            if (contenido.getJSONObject(i).getInt(ConstantesJSON.PISTA_API_KEY_ID) == id) {
                contenido.remove(contenido.getJSONObject(i));
                noEncontrado = false;
            }
        }
        JSONObject pistaJSON = new JSONObject(pista.toJSONString());
        contenido.put(pistaJSON);
        return contenido;
    }

    private static String readFile(final String ruta) throws FileNotFoundException, IOException {

        if (ruta == null || !new File(ruta).exists()) {
            return "";
        }
        final FileReader fr = new FileReader(ruta);
        final BufferedReader bufferedReader = new BufferedReader(fr);

        final StringBuilder stringBuilder = new StringBuilder();
        String line;

        while ((line = bufferedReader.readLine()) != null) {

            stringBuilder.append(line).append('\n');
        }
        fr.close();
        return stringBuilder.toString();
    }

    private static void saveFile(final JSONArray contenido) throws IOException {
        final FileWriter fw = new FileWriter(RUTA_JSON, false);
        final BufferedWriter bw = new BufferedWriter(fw);
        final PrintWriter impresor = new PrintWriter(bw);
        impresor.print(contenido);
        impresor.flush();
        impresor.close();
        bw.close();
        fw.close();
        fw.close();
    }
}