/*
 * Cliente de la herramienta de compartición de archivos MP3
 * y edición de metadatos.
 * Aplicaciones y Servicios Avanzados en Internet,
 * Máster en Ingeniería Informática, curso 2014-2015,
 * Universidad de Valladolid.
 *
 * # Autores
 * Cerezo Redondo,		Borja.
 * Roda Suárez,			Daniel.
 * Tejedor García,		Cristian.
 */
package metamp3asai1415.metamp3lanzador;

import java.util.List;
import metamp3asai1415.metamp3client.Comunicacion;
import metamp3asai1415.metamp3client.Pista;

/**
 * Escenario 2.<br/>Ejecutar cada escenario en una máquina distinta.
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 1.0-FINAL
 */
public class Escenario2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        try {
            System.out.println("\n\nEl cliente 2 se encuentra en la zona 1, se conecta por primera vez online y muestra por pantalla la lista de archivos en el servidor.");
            FuncionesComunes.esperaPulsacion();
            List<Pista> canciones = Comunicacion.verLista();
            System.out.println("El usuario selecciona el tema \"Under Presure\" de \"Queen\" solicitando su descarga");
            FuncionesComunes.esperaPulsacion();
            Pista pista = FuncionesComunes.buscaPista(canciones, 123123);
            if (pista != null) {
                Comunicacion.descargarArchivo(pista, 1);
                System.out.println("Metadatos en la base de datos local:");
                canciones = Comunicacion.verListaLocal();
                System.out.println("El 28 de enero de 2015 el usuario modifica los metadatos del archivo descargado y sincroniza con el servidor");
                FuncionesComunes.esperaPulsacion();
                pista = FuncionesComunes.buscaPista(canciones, 123123);
                pista = FuncionesComunes.modificaPista(pista, "Bajo presion", "La Reina", 2, "2015-01-28 19:55:02", "2015-01-28 19:55:03", "2015-01-28 19:55:04");
                Comunicacion.sincronizar(pista);
                System.out.println("Metadatos en el servidor:");
                Comunicacion.verLista();
                System.out.println("Metadatos en la base de datos local:");
                Comunicacion.verListaLocal();
            } else {
                System.err.printf("ERROR, Id de canción predefinido erróneo");
            }
        } catch (Exception ex) {
            System.err.printf(ex.getMessage());
            ex.printStackTrace();
        }
    }
}
