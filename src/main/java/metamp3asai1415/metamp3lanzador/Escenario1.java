/*
 * Cliente de la herramienta de compartición de archivos MP3
 * y edición de metadatos.
 * Aplicaciones y Servicios Avanzados en Internet,
 * Máster en Ingeniería Informática, curso 2014-2015,
 * Universidad de Valladolid.
 *
 * # Autores
 * Cerezo Redondo,		Borja.
 * Roda Suárez,			Daniel.
 * Tejedor García,		Cristian.
 */
package metamp3asai1415.metamp3lanzador;

import java.util.List;
import metamp3asai1415.metamp3client.Comunicacion;
import metamp3asai1415.metamp3client.Pista;

/**
 *
 * Escenario 1.<br/>Ejecutar cada escenario en una máquina distinta.
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 1.0-FINAL
 */
public class Escenario1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        try {
            System.out.println("\n\nEl cliente 1 se encuentra en la zona 2, se conecta por primera vez online y muestra por pantalla la lista de archivos en el servidor.");
            FuncionesComunes.esperaPulsacion();
            List<Pista> canciones = Comunicacion.verLista();
            System.out.println("El usuario selecciona el tema \"Under Presure\" de \"Queen\" solicitando su descarga");
            FuncionesComunes.esperaPulsacion();
            Pista pista = FuncionesComunes.buscaPista(canciones, 123123);
            if (pista != null) {
                Comunicacion.descargarArchivo(pista, 2);
                System.out.println("Metadatos en la base de datos local:");
                Comunicacion.verListaLocal();
            } else {
                System.err.printf("ERROR, Id de canción predefinido erróneo");
            }
        } catch (Exception ex) {
            System.err.printf(ex.getMessage());
            ex.printStackTrace();
        }
    }
}
