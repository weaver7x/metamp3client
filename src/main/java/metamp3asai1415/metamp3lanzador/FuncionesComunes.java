/*
 * Cliente de la herramienta de compartición de archivos MP3
 * y edición de metadatos.
 * Aplicaciones y Servicios Avanzados en Internet,
 * Máster en Ingeniería Informática, curso 2014-2015,
 * Universidad de Valladolid.
 * 
 * # Autores
 * Cerezo Redondo,		Borja.
 * Roda Suárez,			Daniel.
 * Tejedor García,		Cristian.
 */
package metamp3asai1415.metamp3lanzador;

import java.io.IOException;
import java.util.List;
import metamp3asai1415.metamp3client.Pista;

/**
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public class FuncionesComunes {

    public static void esperaPulsacion() throws IOException {
        System.out.println("\t-----Pulse salto de línea para continuar-----\n");
        System.in.read();
    }

    public static Pista buscaPista(List<Pista> lista, int idPista) throws IOException {
        int tamLista = lista.size();
        for (Pista pista : lista) {
            if (pista.getId() == idPista) {
                return pista;
            }
        }
        return null;
    }
    
    public static Pista modificaPista(Pista pista,
            final String nombreCancion, final String artista, final int idGenero,
            final String fechaModiNombreCancion, final String fechaModiArtista, final String fechaModiGenero) {

                pista.setNombreCancion(nombreCancion);
                pista.setFechaModificacionNombreCancion(fechaModiNombreCancion);
                pista.setArtista(artista);
                pista.setFechaModificacionArtista(fechaModiArtista);
                pista.setIdGenero(idGenero);
                pista.setFechaModificacionGenero(fechaModiGenero);
        return pista;
    }
}
