/*
 * Cliente de la herramienta de compartición de archivos MP3
 * y edición de metadatos.
 * Aplicaciones y Servicios Avanzados en Internet,
 * Máster en Ingeniería Informática, curso 2014-2015,
 * Universidad de Valladolid.
 *
 * # Autores
 * Cerezo Redondo,		Borja.
 * Roda Suárez,			Daniel.
 * Tejedor García,		Cristian.
 */
package metamp3asai1415.metamp3lanzador;

import java.util.List;
import metamp3asai1415.metamp3client.Comunicacion;
import metamp3asai1415.metamp3client.Pista;

/**
 * Escenario 4.Inicial.<br/>Ejecutar cada escenario en una máquina distinta.
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 1.0-FINAL
 */
public class Escenario4ClienteInicial {

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        try {
            System.out.println("\n\nEl cliente 4 se encuentra en la zona 1, se conecta por primera vez online y muestra por pantalla la lista de archivos en el servidor.");
            FuncionesComunes.esperaPulsacion();
            List<Pista> canciones = Comunicacion.verLista();
            System.out.println("El usuario selecciona el tema \"Under Presure\" de \"Queen\" solicitando su descarga");
            FuncionesComunes.esperaPulsacion();
            Pista pista = FuncionesComunes.buscaPista(canciones, 123123);
            if (pista != null) {
                Comunicacion.descargarArchivo(pista, 1);
                System.out.println("Metadatos en la base de datos local:");
                canciones = Comunicacion.verListaLocal();
                System.out.println("El 30 de enero de 2015 cliente se desconecta y edita el artista del archivo descargado en local");
                System.out.println("···(Si está realizando el escenario 4, puede empezar a ejecutar el cliente secundario)···");
                FuncionesComunes.esperaPulsacion();
                Comunicacion.pasarOffline();
                pista = FuncionesComunes.buscaPista(canciones, 123123);
                pista.setArtista("soy un artista modificado");
                pista.setFechaModificacionArtista("2015-01-30 00:44:22");
                Comunicacion.sincronizar(pista);
                System.out.println("Metadatos en la base de datos local:");
                canciones = Comunicacion.verListaLocal();
                System.out.println("El cliente vuelve a conectarse online, sincronizándose automáticamente todos los archivos que tiene descargados.");
                System.out.println("···(Si está realizando el escenario 4, debería finalizar la ejecución del cliente secundario antes de continuar)···");
                FuncionesComunes.esperaPulsacion();
                Comunicacion.pasarOnline();
                System.out.println("Metadatos en el servidor:");
                Comunicacion.verLista();
                System.out.println("Metadatos en la base de datos local:");
                Comunicacion.verListaLocal();
            } else {
                System.err.printf("ERROR, Id de canción predefinido erróneo");
            }
        } catch (Exception ex) {
            System.err.printf(ex.getMessage());
            ex.printStackTrace();
        }
    }
}
