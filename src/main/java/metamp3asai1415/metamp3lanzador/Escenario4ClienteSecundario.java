/*
 * Cliente de la herramienta de compartición de archivos MP3
 * y edición de metadatos.
 * Aplicaciones y Servicios Avanzados en Internet,
 * Máster en Ingeniería Informática, curso 2014-2015,
 * Universidad de Valladolid.
 *
 * # Autores
 * Cerezo Redondo,		Borja.
 * Roda Suárez,			Daniel.
 * Tejedor García,		Cristian.
 */
package metamp3asai1415.metamp3lanzador;

import java.util.List;
import metamp3asai1415.metamp3client.Comunicacion;
import metamp3asai1415.metamp3client.Pista;

/**
 * Escenario 4.Secundario.<br/>Ejecutar cada escenario en una máquina distinta.
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 1.0-FINAL
 */
public class Escenario4ClienteSecundario {

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        try {
            System.out.println("\n\nEl cliente 5 se encuentra en la zona 3, se conecta por primera vez online y muestra por pantalla la lista de archivos en el servidor.");
            FuncionesComunes.esperaPulsacion();
            List<Pista> canciones = Comunicacion.verLista();
            System.out.println("El 30 de enero de 2015 el usuario selecciona el tema \"Under Presure\" de \"Queen\" y edita su nombre de forma remota");
            FuncionesComunes.esperaPulsacion();
            Pista pista = FuncionesComunes.buscaPista(canciones, 123123);
            if (pista != null) {
                pista.setNombreCancion("soy cancion modificada cliente 5");
                pista.setFechaModificacionNombreCancion("2015-01-30 10:55:03");
                Comunicacion.sincronizar(pista);
                System.out.println("Metadatos en el servidor:");
                Comunicacion.verLista();
                System.out.println("Metadatos en la base de datos local:");
                Comunicacion.verListaLocal();
            } else {
                System.err.printf("ERROR, Id de canción predefinido erróneo");
            }
        } catch (Exception ex) {
            System.err.printf(ex.getMessage());
            ex.printStackTrace();
        }
    }
}
