# **MetaMP3Client** #

Cliente de la herramienta de compartición de archivos MP3 y edición de metadatos.
Cada escenario de pruebas debe utilizarse en máquinas distintas, ya que es una aplicación distribuida.



# ** Autores** #

Cerezo Redondo, Borja

Roda Suárez, Daniel

Tejedor García, Cristian 



# ** Compilación** #

    cd <directorioApp>

    mvn package



# ** Ejecución** #

    cd <directorioApp>/ejecutar

    java -cp MetaMP3Client-1.0-FINAL.jar metamp3asai1415.metamp3lanzador.<Nombre_de_la_clase_del_Escenario>



# ** JDK** #

Versión 7 del JDK de Java.


# ** Maven (versión 2)** #

    sudo apt-get install maven